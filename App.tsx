import React, { useEffect, useRef, useState } from "react";
import { Provider } from "react-redux";
import { LogBox } from "react-native";
import NetInfo from "@react-native-community/netinfo";
import Route from "route";
import store from "store";
import NoInternetView from "component/NoInternetView";

export default function App() {
  const _isMounted = useRef(true);
  const [isConnected, setConnected] = useState<boolean>(true);
  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(handleConnectivityChange);
    return () => {
      _isMounted.current = false;
      unsubscribe();
    };
  }, []);

  const handleConnectivityChange = (state) => {
    if (_isMounted.current) {
      setConnected(state?.isConnected);
      console.log("IS MOUNTED :- ", _isMounted.current);
      console.log("STATE :== ", JSON.stringify(state, null, 2));
      console.log("==: IS CONNECTED :== ", state.isConnected);
    }
  };
  LogBox.ignoreAllLogs();
  if (!isConnected) {
    return <NoInternetView />;
  }
  return (
    <Provider store={store}>
      <Route />
    </Provider>
  );
}
