export const Constant = {
  SPACE_PATTERN: new RegExp(/\s/),
};
export const ScreenOptions = {
  headerShown: false,
  gestureEnabled: false,
};
