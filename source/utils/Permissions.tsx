import { Platform } from "react-native";
import {
  checkMultiple,
  check,
  request,
  requestMultiple,
  PERMISSIONS,
  RESULTS,
} from "react-native-permissions";
import Toast from "react-native-simple-toast";
let TOAST_DURATION = Toast.LONG;
let MSG_CAMERA_AUDIO =
    "Camera and record audio permissions hasn't been allowed. To use photos and videos, please give permission to access your phone camera and microphone.",
  MSG_CAMERA_STORAGE =
    "Access storage and camera permissions hasn't been allowed. To use photos and videos, please give permission to access your phone camera and storage.",
  MSG_CAMERA =
    "Camera permission hasn't been allowed. To use photos and videos, please give permission to access your phone camera.",
  MSG_AUDIO =
    "Record audio permission hasn't been allowed. To use videos, please give permission to access your phone microphone.",
  MSG_STORAGE =
    "Access phone storage permission hasn't been allowed. To use photos and videos, please give permission to access your phone storage.",
  MSG_WRITE_CONTACT =
    "Write contact in phone permission hasn't been allowed. To use contacts, please give permission to access your phone contact.",
  MSG_READ_CONTACT =
    "Access phone contact permission hasn't been allowed. To use contacts, please give permission to access your phone contact.",
  MSG_LOCATION =
    "Access phone location permission hasn't been allowed. To use location, please give permission to access your phone location.",
  MSG_HARDWARE =
    "Permission to access hardware was blocked, please grant manually",
  MSG_NOTGRANTED = "Permission not granted",
  MSG_HARDWARE_CAMERA_AUDIO =
    "Hardware to support camera and microphone is not available",
  MSG_HARDWARE_CAMERA_STORAGE =
    "Hardware to support camera and storage is not available",
  MSG_HARDWARE_CAMERA = "Hardware to support camera is not available",
  MSG_HARDWARE_AUDIO = "Hardware to support microphone is not available",
  MSG_HARDWARE_STORAGE = "Hardware to support storage is not available",
  MSG_HARDWARE_CONTACT = "Hardware to support contact is not available",
  MSG_HARDWARE_LOCATION = "Hardware to support location is not available";

export const checkAllPermissionsOnce = async () => {
  try {
    const ios_permission = [
      PERMISSIONS.IOS.CAMERA,
      PERMISSIONS.IOS.MICROPHONE,
      PERMISSIONS.IOS.PHOTO_LIBRARY,
      PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
    ];
    const android_permission = [
      PERMISSIONS.ANDROID.CAMERA,
      PERMISSIONS.ANDROID.RECORD_AUDIO,
      PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
      PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
      PERMISSIONS.ANDROID.WRITE_CONTACTS,
      PERMISSIONS.ANDROID.READ_CONTACTS,
      PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
    ];
    await requestMultiplePermission(
      Platform.OS == "ios" ? ios_permission : android_permission
    );
    return true;
  } catch (e) {}
};

export const checkSinglePermissionContact = async () => {
  return await check(PERMISSIONS.ANDROID.READ_CONTACTS).then((status) => {
    return status;
  });
};

async function checkMultiplePermissions(permissions) {
  return await checkMultiple(permissions).then((statuses) => {
    return statuses;
  });
}
async function checkSinglePermission(permission) {
  return await check(permission).then((status) => {
    return status;
  });
}
export const requestSinglePermission = async (permission) => {
  let granted = false;
  await request(permission).then((result) => {
    if (result === RESULTS.GRANTED) {
      granted = true;
    } else {
      granted = false;
    }
  });
  return granted;
};
export const requestMultiplePermission = async (permission) => {
  const [PERMISSION_1, PERMISSION_2] = permission;
  let granted = false;
  await requestMultiple(permission).then((newStatuses) => {
    if (
      newStatuses[PERMISSION_1] === RESULTS.GRANTED &&
      newStatuses[PERMISSION_2] === RESULTS.GRANTED
    ) {
      granted = true;
    } else if (
      (newStatuses[PERMISSION_1] === RESULTS.GRANTED &&
        newStatuses[PERMISSION_2] === RESULTS.DENIED) ||
      (newStatuses[PERMISSION_1] === RESULTS.DENIED &&
        newStatuses[PERMISSION_2] === RESULTS.GRANTED)
    ) {
      granted = false;
    } else {
      granted = false;
    }
  });
  return granted;
};
export const checkPermissionAudioCamera = async () => {
  let granted = false;
  try {
    const ios_permission = [PERMISSIONS.IOS.CAMERA, PERMISSIONS.IOS.MICROPHONE];
    const android_permission = [
      PERMISSIONS.ANDROID.CAMERA,
      PERMISSIONS.ANDROID.RECORD_AUDIO,
    ];
    let statuses = await checkMultiplePermissions(
      Platform.OS === "ios" ? ios_permission : android_permission
    );
    const [PERMISSION_1, PERMISSION_2] =
      Platform.OS === "ios" ? ios_permission : android_permission;
    if (
      statuses[PERMISSION_1] === RESULTS.UNAVAILABLE ||
      statuses[PERMISSION_2] === RESULTS.UNAVAILABLE
    ) {
      Toast.showWithGravity(
        MSG_HARDWARE_CAMERA_AUDIO,
        TOAST_DURATION,
        Toast.TOP
      );
    } else if (
      statuses[PERMISSION_1] === RESULTS.BLOCKED ||
      statuses[PERMISSION_2] === RESULTS.BLOCKED
    ) {
      if (
        statuses[PERMISSION_1] === RESULTS.BLOCKED &&
        statuses[PERMISSION_2] === RESULTS.BLOCKED
      ) {
        granted = false;
        Toast.showWithGravity(MSG_CAMERA_AUDIO, TOAST_DURATION, Toast.TOP);
      } else if (statuses[PERMISSION_1] === RESULTS.BLOCKED) {
        granted = false;
        Toast.showWithGravity(MSG_CAMERA, TOAST_DURATION, Toast.TOP);
      } else if (statuses[PERMISSION_2] === RESULTS.BLOCKED) {
        granted = false;
        Toast.showWithGravity(MSG_AUDIO, TOAST_DURATION, Toast.TOP);
      } else {
        granted = false;
        Toast.showWithGravity(MSG_HARDWARE, TOAST_DURATION, Toast.TOP);
      }
    } else {
      if (
        statuses[PERMISSION_1] === RESULTS.DENIED &&
        statuses[PERMISSION_2] === RESULTS.DENIED
      ) {
        granted = await requestMultiplePermission(
          Platform.OS == "ios" ? ios_permission : android_permission
        );
      } else if (
        statuses[PERMISSION_1] === RESULTS.GRANTED &&
        statuses[PERMISSION_2] === RESULTS.GRANTED
      ) {
        granted = true;
      } else if (
        statuses[PERMISSION_1] === RESULTS.DENIED ||
        statuses[PERMISSION_2] === RESULTS.DENIED
      ) {
        if (statuses[PERMISSION_1] === RESULTS.DENIED) {
          granted = await requestSinglePermission(PERMISSION_1);
        } else if (statuses[PERMISSION_2] === RESULTS.DENIED) {
          granted = await requestSinglePermission(PERMISSION_2);
        } else {
          granted = false;
          Toast.showWithGravity(MSG_NOTGRANTED, TOAST_DURATION, Toast.TOP);
        }
      } else {
        granted = false;
      }
    }
  } catch (e) {}
  return granted;
};

export const checkPermissionAudio = async () => {
  const ios_permission = PERMISSIONS.IOS.MICROPHONE;
  const android_permission = PERMISSIONS.ANDROID.RECORD_AUDIO;
  let status = await checkSinglePermission(
    Platform.OS === "ios" ? ios_permission : android_permission
  );
  let granted = false;
  if (status === RESULTS.UNAVAILABLE) {
    granted = false;
    Toast.showWithGravity(MSG_HARDWARE_AUDIO, TOAST_DURATION, Toast.TOP);
  } else if (status === RESULTS.BLOCKED) {
    granted = false;
    Toast.showWithGravity(MSG_AUDIO, TOAST_DURATION, Toast.TOP);
  } else {
    if (status === RESULTS.DENIED) {
      granted = await requestSinglePermission(
        Platform.OS === "ios" ? ios_permission : android_permission
      );
    } else if (status === RESULTS.GRANTED) {
      granted = true;
    } else {
      granted = false;
      Toast.showWithGravity(MSG_NOTGRANTED, TOAST_DURATION, Toast.TOP);
    }
  }
  return granted;
};
export const checkPermissionWriteStorage = async () => {
  const android_permission = PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE;
  let status = await checkSinglePermission(android_permission);
  let granted = false;
  if (status === RESULTS.UNAVAILABLE) {
    granted = false;
    Toast.showWithGravity(MSG_HARDWARE_STORAGE, TOAST_DURATION, Toast.TOP);
  } else if (status === RESULTS.BLOCKED) {
    granted = false;
    Toast.showWithGravity(MSG_STORAGE, TOAST_DURATION, Toast.TOP);
  } else {
    if (status === RESULTS.DENIED) {
      granted = await requestSinglePermission(android_permission);
    } else if (status === RESULTS.GRANTED) {
      granted = true;
    } else {
      granted = false;
      Toast.showWithGravity(MSG_NOTGRANTED, TOAST_DURATION, Toast.TOP);
    }
  }
  return granted;
};

export const checkPermissionCamera = async () => {
  const ios_permission = PERMISSIONS.IOS.CAMERA;
  const android_permission = PERMISSIONS.ANDROID.CAMERA;
  let status = await checkSinglePermission(
    Platform.OS === "ios" ? ios_permission : android_permission
  );
  let granted = false;
  if (status === RESULTS.UNAVAILABLE) {
    granted = false;
    Toast.showWithGravity(MSG_HARDWARE_CAMERA, TOAST_DURATION, Toast.TOP);
  } else if (status === RESULTS.BLOCKED) {
    granted = false;
    Toast.showWithGravity(MSG_CAMERA, TOAST_DURATION, Toast.TOP);
  } else {
    if (status === RESULTS.DENIED) {
      granted = await requestSinglePermission(
        Platform.OS === "ios" ? ios_permission : android_permission
      );
    } else if (status === RESULTS.GRANTED) {
      granted = true;
    } else {
      granted = false;
      Toast.showWithGravity(MSG_NOTGRANTED, TOAST_DURATION, Toast.TOP);
    }
  }
  return granted;
};
export const checkPermissionWriteContact = async () => {
  const android_permission = PERMISSIONS.ANDROID.WRITE_CONTACTS;
  let status = await checkSinglePermission(android_permission);
  let granted = false;
  if (status === RESULTS.UNAVAILABLE) {
    granted = false;
    Toast.showWithGravity(MSG_HARDWARE_CONTACT, TOAST_DURATION, Toast.TOP);
  } else if (status === RESULTS.BLOCKED) {
    granted = false;
    Toast.showWithGravity(MSG_WRITE_CONTACT, TOAST_DURATION, Toast.TOP);
  } else {
    if (status === RESULTS.DENIED) {
      granted = await requestSinglePermission(android_permission);
    } else if (status === RESULTS.GRANTED) {
      granted = true;
    } else {
      granted = false;
      Toast.showWithGravity(MSG_NOTGRANTED, TOAST_DURATION, Toast.TOP);
    }
  }
  return granted;
};
export const checkPermissionReadContact = async () => {
  const android_permission = PERMISSIONS.ANDROID.READ_CONTACTS;
  let status = await checkSinglePermission(android_permission);
  let granted = false;
  if (status === RESULTS.UNAVAILABLE) {
    granted = false;
    Toast.showWithGravity(MSG_HARDWARE_CONTACT, TOAST_DURATION, Toast.TOP);
  } else if (status === RESULTS.BLOCKED) {
    granted = false;
    Toast.showWithGravity(MSG_READ_CONTACT, TOAST_DURATION, Toast.TOP);
  } else {
    if (status === RESULTS.DENIED) {
      granted = await requestSinglePermission(android_permission);
    } else if (status === RESULTS.GRANTED) {
      granted = true;
    } else {
      granted = false;
      Toast.showWithGravity(MSG_NOTGRANTED, TOAST_DURATION, Toast.TOP);
    }
  }
  return granted;
};

export const checkPermissionStorageCamera = async () => {
  const android_permission = [
    PERMISSIONS.ANDROID.CAMERA,
    PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
  ];
  let statuses = await checkMultiplePermissions(android_permission);
  let granted = false;
  const [PERMISSION_1, PERMISSION_2] = android_permission;
  if (
    statuses[PERMISSION_1] === RESULTS.UNAVAILABLE ||
    statuses[PERMISSION_2] === RESULTS.UNAVAILABLE
  ) {
    Toast.showWithGravity(
      MSG_HARDWARE_CAMERA_STORAGE,
      TOAST_DURATION,
      Toast.TOP
    );
  } else if (
    statuses[PERMISSION_1] === RESULTS.BLOCKED ||
    statuses[PERMISSION_2] === RESULTS.BLOCKED
  ) {
    if (
      statuses[PERMISSION_1] === RESULTS.BLOCKED &&
      statuses[PERMISSION_2] === RESULTS.BLOCKED
    ) {
      granted = false;
      Toast.showWithGravity(MSG_CAMERA_STORAGE, TOAST_DURATION, Toast.TOP);
    } else if (statuses[PERMISSION_1] === RESULTS.BLOCKED) {
      granted = false;
      Toast.showWithGravity(MSG_CAMERA, TOAST_DURATION, Toast.TOP);
    } else if (statuses[PERMISSION_2] === RESULTS.BLOCKED) {
      granted = false;
      Toast.showWithGravity(MSG_STORAGE, TOAST_DURATION, Toast.TOP);
    } else {
      granted = false;
      Toast.showWithGravity(MSG_HARDWARE, TOAST_DURATION, Toast.TOP);
    }
  } else {
    if (
      statuses[PERMISSION_1] === RESULTS.DENIED &&
      statuses[PERMISSION_2] === RESULTS.DENIED
    ) {
      granted = await requestMultiplePermission(android_permission);
    } else if (
      statuses[PERMISSION_1] === RESULTS.GRANTED &&
      statuses[PERMISSION_2] === RESULTS.GRANTED
    ) {
      granted = true;
    } else if (
      statuses[PERMISSION_1] === RESULTS.DENIED ||
      statuses[PERMISSION_2] === RESULTS.DENIED
    ) {
      if (statuses[PERMISSION_1] === RESULTS.DENIED) {
        granted = await requestSinglePermission(PERMISSION_1);
      } else if (statuses[PERMISSION_2] === RESULTS.DENIED) {
        granted = await requestSinglePermission(PERMISSION_2);
      } else {
        granted = false;
        Toast.showWithGravity(MSG_NOTGRANTED, TOAST_DURATION, Toast.TOP);
      }
    } else {
      granted = false;
    }
  }
  return granted;
};
export const checkPermissionReadStorage = async () => {
  const ios_permission = PERMISSIONS.IOS.PHOTO_LIBRARY;
  const android_permission = PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE;
  let status = await checkSinglePermission(
    Platform.OS === "ios" ? ios_permission : android_permission
  );
  let granted = false;
  if (status === RESULTS.UNAVAILABLE) {
    granted = false;
    Toast.showWithGravity(MSG_HARDWARE_STORAGE, TOAST_DURATION, Toast.TOP);
  } else if (status === RESULTS.BLOCKED) {
    granted = false;
    Toast.showWithGravity(MSG_STORAGE, TOAST_DURATION, Toast.TOP);
  } else {
    if (status === RESULTS.DENIED) {
      granted = await requestSinglePermission(
        Platform.OS == "ios" ? ios_permission : android_permission
      );
    } else if (status === RESULTS.GRANTED) {
      granted = true;
    } else {
      granted = false;
      Toast.showWithGravity(MSG_NOTGRANTED, TOAST_DURATION, Toast.TOP);
    }
  }
  return granted;
};
export const checkPermissionLocation = async () => {
  const ios_permission = PERMISSIONS.IOS.LOCATION_WHEN_IN_USE;
  const android_permission = PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;
  let status = await checkSinglePermission(
    Platform.OS === "ios" ? ios_permission : android_permission
  );
  let granted = false;
  if (status === RESULTS.UNAVAILABLE) {
    Toast.showWithGravity(MSG_HARDWARE_LOCATION, TOAST_DURATION, Toast.TOP);
  } else if (status === RESULTS.BLOCKED) {
    Toast.showWithGravity(MSG_LOCATION, TOAST_DURATION, Toast.TOP);
  } else {
    if (status === RESULTS.DENIED) {
      granted = await requestSinglePermission(
        Platform.OS == "ios" ? ios_permission : android_permission
      );
    } else if (status === RESULTS.GRANTED) {
      granted = true;
    } else {
      granted = false;
      Toast.showWithGravity(MSG_NOTGRANTED, TOAST_DURATION, Toast.TOP);
    }
  }
  return granted;
};
