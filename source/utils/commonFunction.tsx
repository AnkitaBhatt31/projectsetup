import Toast from "react-native-simple-toast";
// import { useNavigation } from "@react-navigation/core";

// type navProps = {
//   navigate: (value: string, params?: object) => void;
//   replace?: (value: string, params?: object) => void;
//   goBack?: () => void;
//   reset?: (data: object) => void;
//   push: (value: string, params?: object) => void;
// };
// export const { navigate, replace, goBack, reset, push } =
//   useNavigation<navProps>();

export function print(...msg: any) {
  __DEV__ && console.log(...msg);
}

export function toast(msg: string) {
  Toast.showWithGravity(msg, Toast.SHORT, Toast.BOTTOM);
}

export async function clearAsyncData(props?: any) {
  try {
  } catch (e) {}
}

export function checkError(res?: any, message?: string) {}
