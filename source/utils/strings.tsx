export const Strings = {
  heading: "Heading",
  title: "Title",
};

export const NAVIGATION = {
  LOGIN: "Login",
  HOME: "Home",
  PROFILE: "Profile",
  DASHBOARD: "Dashboard",
  DETAIL: "Detail",
};

export const ApiPath = {
  BASE_URL: "https://",
  login: "user/login",
  register: "user/register",
  productList: "products",
};

export const ApiError = {
  checkInternet: "Please check your internet connection and try again later",
};

export const Types = {
  loading: "LOADING",
  // Test Reducer
  test: "TEST",
  productList: "PRODUCT_LIST",
  heading: "HEADING",
  // User Reducer
  user: "USER",
};
