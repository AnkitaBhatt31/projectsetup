import axios from "axios";
import { ApiError, ApiPath } from "./strings";
import { print, checkError, toast, clearAsyncData } from "./commonFunction";
import store from "../store";
import { isEmpty } from "underscore";

axios.defaults.timeout = 30000;
axios.defaults.baseURL = ApiPath.BASE_URL;
axios.defaults.headers["Content-Type"] = "application/json";

axios.interceptors.request.use(
  async function (config) {
    const { token } = store.getState().User.user;
    if (!isEmpty(token)) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    return config;
  },
  function (err) {
    return Promise.reject(err);
  }
);

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    if (error?.response?.status === 401) {
      await clearAsyncData();
    }
    return Promise.reject(error);
  }
);

export async function post(path: string, body?: any, params?: any) {
  try {
    const res = await axios.post(path, body, params);
    if (res?.status == 200 || res?.status == 201 || res?.status == 204) {
      return res.data;
    }
  } catch (err) {
    if (!err?.response) {
      toast(ApiError.checkInternet);
    } else {
      const errorMessage = err?.response?.data?.errors?.msg;
      checkError(err?.response, errorMessage);
    }
  }
}

export async function get(path: string, params?: any, token?: string) {
  try {
    const res = await axios.get(path, params);
    if (res?.status == 200 || res?.status == 201 || res?.status == 204) {
      return res.data;
    }
  } catch (err) {
    print("API ERROR :- ", err);
    if (!err.response) {
      toast(ApiError.checkInternet);
    } else {
      const errorMessage = err?.response?.data?.errors?.msg;
      checkError(err?.response, errorMessage);
    }
  }
}

export async function put(path: string, body?: any, params?: any) {
  try {
    const res = await axios.put(path, body, params);
    if (res?.status == 200 || res?.status == 201 || res?.status == 204) {
      return res.data;
    }
  } catch (err) {
    if (!err?.response) {
      toast(ApiError.checkInternet);
    } else {
      const errorMessage = err?.response?.data?.errors?.msg;
      checkError(err?.response, errorMessage);
    }
  }
}

export async function deleteApi(path: string, params?: any) {
  try {
    const res = await axios.delete(path, params);
    return res.data;
  } catch (err) {
    print("API ERROR :- ", err);
    if (!err.response) {
      toast(ApiError.checkInternet);
    } else {
      const errorMessage = err?.response?.data?.errors?.msg;
      checkError(err?.response, errorMessage);
    }
    throw err;
  }
}

export async function patch(path: string, body?: any, params?: any) {
  try {
    const res = await axios.patch(path, body, params);
    if (res?.status == 200 || res?.status == 201 || res?.status == 204) {
      return res.data;
    }
  } catch (err) {
    if (!err.response) {
      toast(ApiError.checkInternet);
    } else {
      const errorMessage = err?.response?.data?.errors?.msg;
      checkError(err?.response, errorMessage);
    }
  }
}
