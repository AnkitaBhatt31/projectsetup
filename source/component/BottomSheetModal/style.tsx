import { StyleSheet } from "react-native";
import { Colors, Metrics } from "theme";

const styles = StyleSheet.create({
  modal: {
    justifyContent: "flex-end",
    margin: 0,
    marginHorizontal: Metrics.rfv(12),
  },
  container: {
    backgroundColor: Colors.bottomSheetDialog,
    borderRadius: 8,
    paddingTop: Metrics.rfv(10),
  },
  title: {
    textAlign: "center",
    paddingVertical: Metrics.rfv(5),
    color: Colors.black,
  },
  line: {
    width: "100%",
    height: 0.5,
    backgroundColor: Colors.black,
  },
  bottomSheetButton: {
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: Metrics.rfv(10),
  },
  bottomSheetText: { color: Colors.danger, fontSize: Metrics.rfv(16) },
  cancelButton: {
    backgroundColor: Colors.white,
    marginVertical: Metrics.rfv(15),
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: Metrics.rfv(13),
    borderRadius: 8,
  },
  cancelText: { color: Colors.primary, fontSize: Metrics.rfv(16) },
});
export default styles;
