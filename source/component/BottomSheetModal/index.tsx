import React from "react";
import {
  View,
  Text,
  GestureResponderEvent,
  TouchableOpacity,
} from "react-native";
import Modal from "react-native-modal";
import styles from "./style";
export type modalProps = {
  isModalVisible: boolean;
  title: string;
  onCancel: (event: GestureResponderEvent) => void;
  onRemove: (event: GestureResponderEvent) => void;
  onDelete: (event: GestureResponderEvent) => void;
};
export default function BottomSheetModal({
  isModalVisible = false,
  title,
  onCancel,
  onRemove,
  onDelete,
}: modalProps) {
  return (
    <Modal
      style={styles.modal}
      isVisible={isModalVisible}
      backdropOpacity={0.5}
      animationOutTiming={1000}
    >
      <View style={styles.container}>
        <Text style={styles.title}>{title}</Text>
        <View style={styles.line} />
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.bottomSheetButton}
          onPress={onRemove}
        >
          <Text style={styles.bottomSheetText}>Remove from Album</Text>
        </TouchableOpacity>
        <View style={styles.line} />
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.bottomSheetButton}
          onPress={onDelete}
        >
          <Text style={styles.bottomSheetText}>Delete</Text>
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.cancelButton}
        onPress={onCancel}
      >
        <Text style={styles.cancelText}>Cancel</Text>
      </TouchableOpacity>
    </Modal>
  );
}
