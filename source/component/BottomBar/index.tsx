import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Colors } from "../../theme";
import Icons from "../Icon";

let array = [
  {
    activeIcon: (
      <Icons
        name="home-lightbulb"
        type="MaterialCommunityIcons"
        size={30}
        style={{
          color: Colors.grey,
          justifyContent: "center",
        }}
      />
    ),
    inactiveIcon: (
      <Icons
        name="home-lightbulb-outline"
        type="MaterialCommunityIcons"
        size={30}
        style={{
          color: Colors.white,
          justifyContent: "center",
        }}
      />
    ),
  },
  {
    activeIcon: (
      <Icons
        name="person"
        type="MaterialIcons"
        size={30}
        style={{
          color: Colors.grey,
          justifyContent: "center",
        }}
      />
    ),
    inactiveIcon: (
      <Icons
        name="person-outline"
        type="MaterialIcons"
        size={30}
        style={{
          color: Colors.white,
          justifyContent: "center",
        }}
      />
    ),
  },
];

export default function BottomBar({ state, descriptors, navigation }) {
  return (
    <View
      style={{
        backgroundColor: Colors.success,
        paddingBottom: 20,
        paddingTop: 10,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
      }}
    >
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: "tabPress",
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate({ name: route.name, merge: true });
          }
        };

        return (
          <TouchableOpacity
            key={index?.toString()}
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            style={{ alignItems: "center" }}
          >
            {isFocused ? array[index].activeIcon : array[index].inactiveIcon}
            <Text
              style={[
                {
                  color: isFocused ? Colors.grey : Colors.white,
                },
              ]}
              numberOfLines={1}
            >
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}
