import React from "react";
import { SafeAreaView } from "react-native";

export type safeAreaProps = {
  children: any;
  backgroundColor?: string;
};

export default function AppSafeAreaView(props: safeAreaProps) {
  const { children, backgroundColor } = props;
  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: backgroundColor ? backgroundColor : "transparent",
      }}
    >
      {children}
    </SafeAreaView>
  );
}
