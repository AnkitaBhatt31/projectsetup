import { StyleSheet } from "react-native";
import { Colors, Fonts, Metrics } from "theme";
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontFamily: Fonts.SemiBold,
    fontSize: Metrics.rfv(18),
    color: Colors.black,
    textAlign: "center",
    paddingHorizontal: Metrics.rfv(15),
  },
});

export default styles;
