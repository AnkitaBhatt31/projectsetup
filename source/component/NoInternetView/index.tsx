import React from "react";
import { View, Text } from "react-native";
import styles from "./style";
// === PROP LESS COMPONENT ===
export default function NoInternetView() {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Oops ! internet connection lost.</Text>
      <Text style={styles.title}>
        We will be right back when connection re establish
      </Text>
    </View>
  );
}
