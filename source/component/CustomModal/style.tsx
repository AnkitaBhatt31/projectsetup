import { StyleSheet } from "react-native";
import { Colors, Metrics } from "theme";

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: Colors.white,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8,
    marginHorizontal: Metrics.rfv(30),
  },
  TopView: {
    paddingVertical: Metrics.rfv(10),
    paddingHorizontal: Metrics.rfv(25),
  },
  title: {
    textAlign: "center",
    fontSize: Metrics.rfv(15),
    color: Colors.black,
    fontWeight: "700",
  },
  subDetail: {
    textAlign: "center",
    fontSize: Metrics.rfv(13),
    color: Colors.secondaryText,
  },
  lineView: {
    width: "100%",
    height: 1,
    backgroundColor: Colors.primary,
  },
  bottomView: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
  },
  bottomPressable: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: Metrics.rfv(10),
    borderColor: Colors.primary,
  },
  cancelText: {
    color: Colors.danger,
    textAlign: "center",
    fontSize: Metrics.rfv(15),
    fontWeight: "900",
  },
  submitText: {
    color: Colors.success,
    textAlign: "center",
    fontSize: Metrics.rfv(15),
    fontWeight: "900",
  },
});
export default styles;
