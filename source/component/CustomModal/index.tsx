import React from "react";
import { View, Text, Pressable, GestureResponderEvent } from "react-native";
import Modal from "react-native-modal";
import styles from "./style";
export type modalProps = {
  isModalVisible: boolean;
  title: string;
  subTitle: string;
  onCancel: (event: GestureResponderEvent) => void;
  onSubmit: (event: GestureResponderEvent) => void;
};
export default function CustomModal({
  isModalVisible = false,
  title,
  subTitle,
  onCancel,
  onSubmit,
}: modalProps) {
  return (
    <Modal
      isVisible={isModalVisible}
      backdropOpacity={0.5}
      animationInTiming={800}
      animationOutTiming={800}
      animationOut={"fadeOut"}
      animationIn={"fadeIn"}
    >
      <View style={styles.modalContainer}>
        <View style={styles.TopView}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.subDetail}>{subTitle}</Text>
        </View>
        <View style={styles.lineView} />
        <View style={styles.bottomView}>
          <Pressable
            style={[styles.bottomPressable, { borderRightWidth: 1 }]}
            onPress={onCancel}
          >
            <Text style={styles.cancelText}>Cancel</Text>
          </Pressable>
          <Pressable style={styles.bottomPressable} onPress={onSubmit}>
            <Text style={styles.submitText}>Submit</Text>
          </Pressable>
        </View>
      </View>
    </Modal>
  );
}
