import React, { useEffect, useState, useMemo, memo } from "react";
import { View } from "react-native";
import FastImage from "react-native-fast-image";
import Icons from "component/Icon";
import { isEmpty } from "lodash";
import styles from "./style";
import { Images } from "theme";

export type imageProps = {
  style: object;
  source: object;
  onError?: Function;
  preloadImages?: any[];
};

const ImageView = memo(
  ({ style, source, onError, preloadImages }: imageProps) => {
    const [isLoading, setLoading] = useState(true);
    const [isErrored, setIsErrored] = useState(false);

    function displayIcon() {
      return (
        <View style={[style, styles.iconView]}>
          <Icons type={"Ionicons"} name={"image-outline"} size={20} />
        </View>
      );
    }
    function displayPlaceHolder() {
      return (
        <FastImage
          source={Images.addressNew}
          style={[style, styles.image]}
          resizeMode={FastImage.resizeMode.cover}
          onError={() => {
            setLoading(false);
            setIsErrored(true);
            onError && onError();
          }}
          onLoad={(e) => {
            setLoading(false);
          }}
        />
      );
    }

    useEffect(() => {
      if (preloadImages && !isEmpty(preloadImages)) {
        FastImage.preload(preloadImages);
      }
    }, []);

    const CachedImageMemoized = useMemo(() => {
      return (
        <FastImage
          source={source}
          style={[style, styles.image]}
          resizeMode={FastImage.resizeMode.cover}
          onError={() => {
            setLoading(false);
            setIsErrored(true);
            onError && onError();
          }}
          onLoad={(e) => {
            setLoading(false);
          }}
        />
      );
    }, [style, source]);

    return (
      <View style={[style, styles.mainView, { backgroundColor: "#D3D3D3" }]}>
        {CachedImageMemoized}
        {isLoading && displayIcon()}
        {isErrored && displayPlaceHolder()}
      </View>
    );
  }
);
export default ImageView;
