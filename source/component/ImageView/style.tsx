import { StyleSheet } from "react-native";

export default StyleSheet.create({
  mainView: { alignItems: "center", backgroundColor: "#D3D3D3" },
  iconView: {
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
  },
  image: { position: "absolute", zIndex: -1 },
});
