import React, { useState } from "react";
import { View, TextInput } from "react-native";
import { Colors } from "theme";

export default function Input(props: any) {
  const [hide, setHide] = useState<boolean>(props.secure);
  function toggleHide() {
    setHide(!hide);
  }
  const onChange = (value: string) => {
    props.onChange(value);
  };
  return (
    <View style={props.style}>
      <TextInput
        style={props.inpStyle}
        placeholder={props.placeholder}
        secureTextEntry={hide}
        returnKeyType={props.returnKeyType}
        onChangeText={onChange}
        value={props.value}
        autoComplete="off"
        keyboardType={props.keyboardType || "ascii-capable"}
        maxLength={props.maxLength}
        textContentType="password"
        autoCapitalize={props.autoCapitalize || "none"}
        placeholderTextColor={Colors.grey}
      />
    </View>
  );
}
