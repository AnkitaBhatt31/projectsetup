import React, { useEffect } from "react";
import { StatusBar } from "react-native";
export type statusBarProps = {
  isTransparent?: boolean;
  backgroundColor?: string;
  contentType?: any;
};
export default function AppStatusBar(props: statusBarProps) {
  const { isTransparent, backgroundColor, contentType } = props;

  useEffect(() => {
    StatusBar.setBarStyle(contentType, true);
  }, [contentType]);

  return (
    <StatusBar
      translucent={isTransparent ? isTransparent : false}
      animated={true}
      barStyle={contentType ? contentType : "default"}
      backgroundColor={backgroundColor}
    />
  );
}
