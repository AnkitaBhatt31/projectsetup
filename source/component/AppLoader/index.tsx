import React from "react";
import { ActivityIndicator, View } from "react-native";
import Colors from "theme/colors";
import styles from "./style";
export type loadingProps = {
  loading: boolean;
  title?: string;
};

export default function Loader(props: loadingProps) {
  const { loading } = props;
  return (
    loading && (
      <View style={styles.container}>
        <ActivityIndicator
          animating={loading}
          size="large"
          color={Colors.black}
        />
      </View>
    )
  );
}
