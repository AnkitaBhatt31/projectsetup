import { createRef } from "react";
import { StackActions } from "@react-navigation/native";
export const navigationRef = createRef<any>();

const navigate = (name: any, params?: object) => {
  if (navigationRef.current) {
    return navigationRef?.current?.navigate(name, {
      params,
    });
  } else {
    console.info(name);
  }
};

const push = (name: any, params?: object) => {
  if (navigationRef.current) {
    return navigationRef.current.dispatch(StackActions.push(name, params));
  }
};

function goBack() {
  if (navigationRef.current && navigationRef?.current.canGoBack()) {
    navigationRef?.current?.goBack();
  }
}

const popToTop = () => {
  return navigationRef?.current?.dispatch(StackActions.popToTop());
};

const reset = (name: any, params?: object) => {
  if (navigationRef.current) {
    return navigationRef?.current?.reset({
      index: 0,
      routes: [
        {
          name: name,
          params: params,
        },
      ],
    });
  }
};

const replace = (name: any, params?: object) => {
  if (navigationRef.current) {
    return navigationRef.current?.dispatch(StackActions.replace(name, params));
  }
};

export { navigate, push, goBack, popToTop, reset, replace };
