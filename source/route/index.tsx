import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { NAVIGATION } from "utils/strings";
import { ScreenOptions } from "utils/constant";
import { navigationRef } from "./NavigationService";
import TabStack from "./TabStack";
const Stack = createStackNavigator();

export default function Route() {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator>
        <Stack.Screen
          name={NAVIGATION.HOME}
          component={TabStack}
          options={ScreenOptions}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
