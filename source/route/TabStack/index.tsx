import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import BottomBar from "component/BottomBar";
import Home from "screens/Home";
import Profile from "screens/Profile";
import { ScreenOptions } from "utils/constant";
import { NAVIGATION } from "utils/strings";

const Tab = createBottomTabNavigator();
export default function TabStack() {
  return (
    <Tab.Navigator
      initialRouteName={NAVIGATION.HOME}
      tabBar={(props) => <BottomBar {...props} />}
    >
      <Tab.Screen
        name={NAVIGATION.HOME}
        component={Home}
        options={ScreenOptions}
      />
      <Tab.Screen
        name={NAVIGATION.PROFILE}
        component={Profile}
        options={ScreenOptions}
      />
    </Tab.Navigator>
  );
}
