import React, { useEffect, useState } from "react";
import { View, Text, Image, Pressable } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "store";
import styles from "./style";
import {
  AppSafeAreaView,
  AppLoader,
  ImageView,
  CustomModal,
  BottomSheetModal,
} from "component";
import { Colors, Fonts, Metrics, Images, CommonStyle } from "theme";
import { BackIconBlack } from "theme/svg";
import { getTestData } from "store/action/test.action";
import { NAVIGATION } from "utils/strings";
import {
  navigate,
  reset,
  replace,
  goBack,
  push,
} from "route/NavigationService";

export default function Dashboard() {
  const dispatch = useDispatch<AppDispatch>();
  const { test, loading } = useSelector((state: RootState) => state?.STORE);
  const [name, setName] = useState<string>();
  const [isVisible, setVisible] = useState<boolean>(false);
  const [isBottomVisible, setBottomVisible] = useState<boolean>(false);

  useEffect(() => {
    setName(test?.title);
  }, [test, loading]);

  useEffect(() => {
    async function myFunction() {
      dispatch(getTestData());
    }
    myFunction();
  }, []);

  return (
    <AppSafeAreaView backgroundColor={Colors.white}>
      <CustomModal
        isModalVisible={isVisible}
        title={"lorem ipsum test title"}
        subTitle={
          "It has survived not only five centuries. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        }
        onCancel={() => setVisible(false)}
        onSubmit={() => setVisible(false)}
      />
      <BottomSheetModal
        isModalVisible={isBottomVisible}
        title={"are you sure? want to delete this photo from this page ?"}
        onCancel={() => setBottomVisible(false)}
        onRemove={() => setBottomVisible(false)}
        onDelete={() => setBottomVisible(false)}
      />
      {!loading ? (
        <View style={CommonStyle.container}>
          <Text style={[CommonStyle.header, styles.headerText]}>DASHBOARD</Text>
          <View style={styles.container}>
            <Text style={[styles.heading, { fontFamily: Fonts.Bold }]}>
              1.bold{"  "}
              <Text style={[styles.heading, { fontFamily: Fonts.SemiBold }]}>
                2.semi bold{"  "}
              </Text>
              <Text style={[styles.heading, { fontFamily: Fonts.BoldItalic }]}>
                3.bold italic{"  "}
              </Text>
              <Text style={[styles.heading, { fontFamily: Fonts.ExtraBold }]}>
                4.extra bold{"  "}
              </Text>
              <Text style={[styles.heading, { fontFamily: Fonts.Italic }]}>
                5.italic{"  "}
              </Text>
              <Text style={[styles.heading, { fontFamily: Fonts.Light }]}>
                6.light{"  "}
              </Text>
              <Text style={[styles.heading, { fontFamily: Fonts.Medium }]}>
                7.medium{"  "}
              </Text>
            </Text>
            <View style={styles.borderView} />
            <Text style={CommonStyle.header}>Header</Text>
            <Text style={CommonStyle.subHeader}>Sub Header</Text>
            <Text style={CommonStyle.regularText}>Regular Text. test 1234</Text>
            <ImageView source={Images.addressNew} style={styles.image} />
            <Pressable
              onPress={() => navigate(NAVIGATION.DETAIL, { test: "test" })}
            >
              <Text style={styles.goToProfile}>Navigate to Detail</Text>
            </Pressable>
            <Pressable
              onPress={() => push(NAVIGATION.DETAIL, { name: "test" })}
            >
              <Text style={styles.goToProfile}>Push To Detail</Text>
            </Pressable>
            <Pressable
              onPress={() => reset(NAVIGATION.DETAIL, { test: "test" })}
            >
              <Text style={styles.goToProfile}>Reset To Detail</Text>
            </Pressable>
            <Pressable
              onPress={() => replace(NAVIGATION.DETAIL, { name: "test" })}
            >
              <Text style={styles.goToProfile}>Replace To Detail</Text>
            </Pressable>
            <Pressable onPress={() => setVisible(true)}>
              <Text style={styles.goToProfile}>Open Modal</Text>
            </Pressable>
            <Pressable onPress={() => setBottomVisible(true)}>
              <Text style={styles.goToProfile}>Open Bottom Sheet Modal</Text>
            </Pressable>
          </View>
        </View>
      ) : (
        <AppLoader loading={true} />
      )}
    </AppSafeAreaView>
  );
}
