import { StyleSheet } from "react-native";
import { Colors, Metrics, Fonts } from "theme";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    justifyContent: "center",
    alignItems: "center",
  },
  headerView: { flexDirection: "row", marginVertical: Metrics.rfv(15) },
  headerText: {
    textAlign: "center",
    paddingVertical: Metrics.rfv(10),
  },
  heading: {
    fontSize: Metrics.rfv(13),
    marginHorizontal: Metrics.rfv(10),
    width: Metrics.screenWidth,
    textAlign: "justify",
    color: Colors.black,
    paddingHorizontal: Metrics.rfv(20),
  },
  borderView: {
    height: Metrics.rfv(5),
    width: Metrics.rfv(30),
    backgroundColor: Colors.black,
    marginBottom: Metrics.rfv(20),
  },
  image: {
    height: Metrics.rfv(80),
    width: Metrics.rfv(80),
    backgroundColor: Colors.white,
  },
  goToProfile: {
    fontSize: Metrics.rfv(15),
    color: Colors.black,
    fontFamily: Fonts.Bold,
    textDecorationLine: "underline",
  },
});
export default styles;
