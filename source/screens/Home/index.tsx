import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NAVIGATION } from "utils/strings";
import { ScreenOptions } from "utils/constant";
import Detail from "./Detail";
import Dashboard from "./Dashboard";
const Stack = createStackNavigator();

export default function Route() {
  return (
    <Stack.Navigator initialRouteName={NAVIGATION.DASHBOARD}>
      <Stack.Screen
        name={NAVIGATION.DASHBOARD}
        component={Dashboard}
        options={ScreenOptions}
      />
      <Stack.Screen
        name={NAVIGATION.DETAIL}
        component={Detail}
        options={ScreenOptions}
      />
    </Stack.Navigator>
  );
}
