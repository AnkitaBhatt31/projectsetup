import React from "react";
import { View, Text, Pressable } from "react-native";
import styles from "./style";
import { AppSafeAreaView } from "component";
import { Colors, Metrics, CommonStyle } from "theme";
import { BackIconBlack } from "theme/svg";
import { goBack } from "route/NavigationService";

export default function Detail() {
  return (
    <AppSafeAreaView backgroundColor={Colors.white}>
      <View style={CommonStyle.container}>
        <Pressable style={styles.headerView} onPress={() => goBack()}>
          <BackIconBlack height={Metrics.rfv(20)} width={Metrics.rfv(20)} />
          <Text style={styles.headerText}>DETAIL</Text>
        </Pressable>
      </View>
    </AppSafeAreaView>
  );
}
