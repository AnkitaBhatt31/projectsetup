import { StyleSheet } from "react-native";
import { Colors, Metrics, Fonts } from "theme";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    justifyContent: "center",
    alignItems: "center",
  },
  headerView: { flexDirection: "row", marginVertical: Metrics.rfv(20) },
  headerText: {
    flex: 1,
    fontSize: Metrics.rfv(15),
    color: Colors.black,
    textAlign: "center",
  },
});
export default styles;
