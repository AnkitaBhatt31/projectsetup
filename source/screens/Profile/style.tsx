import { StyleSheet } from "react-native";
import { Colors, Fonts, Metrics } from "theme";

const styles = StyleSheet.create({
  profileHeader: {
    flex: 1,
    textAlign: "center",
    fontSize: Metrics.rfv(17),
    color: Colors.primary,
    fontFamily: Fonts.Medium,
  },
});

export default styles;
