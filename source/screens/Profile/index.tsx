import React from "react";
import { View, Text, Pressable } from "react-native";
import { useNavigation } from "@react-navigation/native";
import styles from "./style";
import { AppSafeAreaView } from "component";
import { Colors, Metrics, CommonStyle } from "theme";
import { BackIconBlack } from "theme/svg";

type profileNavProps = {
  goBack: () => void;
};
export default function Profile() {
  const { goBack } = useNavigation<profileNavProps>();
  return (
    <AppSafeAreaView backgroundColor={Colors.white}>
      <View style={CommonStyle.container}>
        <Text style={styles.profileHeader}>PROFILE SCREEN</Text>
      </View>
    </AppSafeAreaView>
  );
}
