import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import myReducer from './reducer';

const store = createStore(myReducer, applyMiddleware(thunk));

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export default store;
