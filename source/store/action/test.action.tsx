import {get, post} from '../../utils/apiRequest';
import {ApiPath, Types} from '../../utils/strings';

export const setTestData = (payload: object) => ({type: Types.test, payload});
export const setLoading = (payload: boolean) => ({
  type: Types.loading,
  payload,
});

export const getTestData = (data?: any) => async (dispatch: any) => {
  try {
    dispatch(setLoading(true));
    const res: any = await get(ApiPath.productList);
    dispatch(setTestData(res));
    dispatch(setLoading(false));
  } catch (err) {
    dispatch(setLoading(false));
  }
};

export const login = (data?: object) => async (dispatch: any) => {
  try {
    dispatch(setLoading(true));
    const res: any = await post(ApiPath.login, data);
    dispatch(setTestData(res));
    dispatch(setLoading(false));
  } catch (err) {
    dispatch(setLoading(false));
  }
};
