import {CombinedState, combineReducers} from 'redux';
import testReducer from './test.reducer';
import UserReducer from './user.reducer';

const rootReducer = combineReducers({
  STORE: testReducer,
  User: UserReducer,
});

export default (
  state:
    | CombinedState<{
        STORE:
          | {test: any; productList: any[]; heading: string; loading: boolean}
          | {loading: any; test: object; productList: any[]; heading: string}
          | {heading: any; test: object; productList: any[]; loading: boolean};
        User: {user: any; loading: boolean} | {loading: any; user: object};
      }>
    | undefined,
  action: any,
) => rootReducer(state, action);
