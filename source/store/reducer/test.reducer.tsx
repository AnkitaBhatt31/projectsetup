import {Types} from '../../utils/strings';
interface ReducerState {
  test: object;
  productList: any[];
  heading: string;
  loading: boolean;
}
const INITIAL_STATE = {
  test: {},
  productList: [],
  heading: '',
  loading: false,
};
function TestReducer(state: ReducerState = INITIAL_STATE, action: any) {
  switch (action.type) {
    case Types.productList:
      return {
        ...state,
        productList: [...state.productList, action?.payload], // OR
        // productList: action?.payload,
      };
    case Types.test:
      return {
        ...state,
        test: {...INITIAL_STATE?.test, ...action?.payload},
      };
    case Types.loading:
      return {
        ...state,
        loading: action?.payload,
      };
    case Types.heading:
      return {
        ...state,
        heading: action?.payload,
      };
    default:
      return state;
  }
}
export default TestReducer;
