import {Types} from '../../utils/strings';
interface ReducerState {
  user: object;
  loading: boolean;
}
const INITIAL_STATE = {
  user: {},
  loading: false,
};
function UserReducer(state: ReducerState = INITIAL_STATE, action: any) {
  switch (action.type) {
    case Types.user:
      return {
        ...state,
        user: {...INITIAL_STATE?.user, ...action?.payload},
      };
    case Types.loading:
      return {
        ...state,
        loading: action?.payload,
      };
    default:
      return state;
  }
}
export default UserReducer;
