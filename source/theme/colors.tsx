const Colors = {
  black: "#000000",
  secondaryText: "#B01010",
  blackTransparent: "rgba(0, 0, 0, 0.3)",
  white: "#ffffff",
  grey: "#eeeeee",
  primary: "#2084D1",
  darkPrimary: "#0A54A4",
  danger: "#FF0000",
  dangerLight: "#CB2A2A",
  success: "#159878",
  warning: "#ED724C",
  border: "#4E4E4E",
  button: "#7B6DA8",
  buttonDisable: "#E6E0F3",
  transparent: "#00000000",
  bottomSheetDialog: "#EFEFEF",
};
export default Colors;
