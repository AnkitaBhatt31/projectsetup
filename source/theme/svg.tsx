import React from 'react';
import Svg, {Path} from 'react-native-svg';

export const BackIconBlack = (props: any) => (
  <Svg viewBox="0 0 13.503 23.619" {...props}>
    <Path
      data-name="Icon ionic-ios-arrow-back"
      d="M15.321 18l8.937-8.93a1.688 1.688 0 00-2.391-2.384L11.742 16.8a1.685 1.685 0 00-.049 2.327L21.86 29.32a1.688 1.688 0 002.391-2.384z"
      transform="translate(-11.251 -6.194)"
      fill="#000"
    />
  </Svg>
);

export default {BackIconBlack};
