import { StyleSheet } from "react-native";
import { Colors, Metrics, Fonts } from "./index";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  flatListContainer: {
    paddingBottom: Metrics.rfv(30),
    marginHorizontal: Metrics.rfv(10),
  },

  header: {
    fontSize: Metrics.rfv(16),
    color: Colors.black,
    fontFamily: Fonts.SemiBold,
    lineHeight: Metrics.rfv(18),
  },
  subHeader: {
    fontSize: Metrics.rfv(14),
    lineHeight: Metrics.rfv(15),
    color: Colors.black,
    fontFamily: Fonts.SemiBold,
  },
  regularText: {
    fontSize: Metrics.rfv(13),
    lineHeight: Metrics.rfv(15),
    color: Colors.black,
    fontFamily: Fonts.Regular,
  },
});
export default styles;
