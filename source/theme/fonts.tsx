const Fonts = {
  Bold: 'RobotoSerif-Bold',
  BoldItalic: 'RobotoSerif-BoldItalic',
  ExtraBold: 'RobotoSerif-ExtraBold',
  Italic: 'RobotoSerif-Italic',
  Light: 'RobotoSerif-Light',
  Medium: 'RobotoSerif-Medium',
  Regular: 'RobotoSerif-Regular',
  SemiBold: 'RobotoSerif-SemiBold',
};
export default Fonts;
