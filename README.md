# ProjectSetUp

## Project Description

This Project is made for ideal project structure with all .tsx file. This project contains project folders like assets component, route, screens, store, theme, utils into main source folder.

## Theming

### Colors, Dimensions, Fonts, Images

Apply colors from Colors.tsx
import import {Colors, Metrics, Fonts, Images} from 'theme';
usage:- Colors.white,
Metrics.rfv(10),
Metrics.screenWidth,
Fonts.Bold,
Image.addressNew

### How do I get set up?

1. Clone this project from repository.
2. Then simply open your terminal
3. Run below command `yarn install` after Successfully install, install pods with following command - `cd ios; pod install; cd ..`

## How to Run the Project

1. Open the project directory in to terminal
2. Run and build for either OS
   _ Run iOS app
   `yarn ios`
   _ Run Android app
   `yarn android` terminal.

**Libraries used for UI:**

- [@types/react-native](),[axios](),[react-native-gesture-handler](),[react-native-safe-area-context](),[react-native-simple-toast](),[react-native-svg](),[react-redux](),[redux-thunk](),[underscore]()

### Extra

- Some basic components (`AppSafeAreaView`, `AppStatusBar`, `Input`, `AppLoader`)

- **Libraries used for navigation:**

-[@react-navigation](https://github.com/react-navigation/react-navigation)

### Contribution guidelines

- Writing tests
- Code review
- Other guidelines

### Who do I talk to?

- Repo owner or admin
- Other community or team contact

## Troubleshoot Notes

- There are no known issues for run or build processes right now.
