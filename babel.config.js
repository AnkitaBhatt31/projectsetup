module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'babel-plugin-module-resolver',
      {
        root: ['.'],
        extensions: ['.ts', '.tsx', '.js', '.ios.js', '.android.js'],
        alias: {
          component: './source/component',
          route: './source/route',
          screens: './source/screens',
          store: './source/store',
          theme: './source/theme',
          utils: './source/utils',
          // assets: '/source/assets',
          // appImages: './source/assets/images',
          // appImages: './source/assets/images',
          // svg: './source/assets/svg',
        },
      },
    ],
  ],
};
